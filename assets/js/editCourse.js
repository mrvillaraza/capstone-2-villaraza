let formSubmit = document.querySelector('#editCourse');
let params = new URLSearchParams(window.location.search);
let token = localStorage.getItem('token');

let courseId = params.get('courseId');
console.log(courseId);

let courseName = document.querySelector('#courseName');
let coursePrice = document.querySelector('#coursePrice');
let courseDescription = document.querySelector('#courseDescription');
let courseNameOld, coursePriceOld, courseDescriptionOld;

fetch(`https://fathomless-chamber-66302.herokuapp.com/api/courses/${courseId}`, {
	method: 'POST'
})
.then(res => res.json())
.then(data => {
	console.log(data);

	courseName.placeholder = data.name;
	coursePrice.placeholder = data.price;
	courseDescription.placeholder = data.description;

	courseNameOld = data.name;
	coursePriceOld = data.price;
	courseDescriptionOld = data.description;

	formSubmit.addEventListener('submit', (e) => {
		
		e.preventDefault();

		if (courseName.value === '') {
			courseName.value = courseNameOld;
		}

		if (courseDescription.value === '') {
			courseDescription.value = courseDescriptionOld;
		}

		if (coursePrice.value === '') {
			coursePrice.value = coursePriceOld;
		}

		fetch('https://fathomless-chamber-66302.herokuapp.com/api/courses', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json' ,
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: courseName.value,
				description: courseDescription.value,
				price: coursePrice.value,
				courseId: courseId
			})
		})
		.then(data => {
			console.log(data);
			alert('Course details have been updated!')

			// reload edit page
			location.reload();
			
		})

	})

})




