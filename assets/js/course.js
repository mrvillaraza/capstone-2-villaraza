// retrieve the user information for isAdmin
let adminUser = localStorage.getItem('isAdmin');

// retrieve course information
console.log(window.location.search);

// instatiate a URLSearchParams object so methods can be executeed to access the parameters
let params = new URLSearchParams(window.location.search);

console.log(params);
console.log(params.has('courseId'));
console.log(params.get('courseId'));

let courseId = params.get('courseId');

let courseName = document.querySelector('#courseName');
let courseDesc = document.querySelector('#courseDesc');
let coursePrice = document.querySelector('#coursePrice');
let courseEnrollees = document.querySelector('#courseEnrollees');
let enrollContainer = document.querySelector('#enrollContainer');
let token = localStorage.getItem('token');
let enrollees;

fetch(`https://fathomless-chamber-66302.herokuapp.com/api/courses/${courseId}`, {
	method: 'POST'
})
.then(res => res.json())
.then(data => {
	
	console.log(data);

	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = data.price;
	enrollees = data.enrollees;
	
	if (adminUser === 'false' || !adminUser) {

		enrollContainer.innerHTML = 
		`
			<button id="enrollButton" class="btn btn-block btn-primary"> Enroll </button>

		`

		document.querySelector('#enrollButton').addEventListener('click', () => {

			fetch('https://fathomless-chamber-66302.herokuapp.com/api/users/enroll', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
					//line above produces a Bearer entry on the terminal
				},
				body: JSON.stringify({
					courseId: courseId
				})
			})
			.then(res => res.json())
			.then(data => {
				
				console.log(data);

				if (data === true) {

					alert('Thank you for enrolling! See you in class!');
					window.location.replace('courses.html');

				} else {
					alert('something went wrong');
				}

			})

		})

	} else {
		// end goal is to inject the enrollees into courseEnrollees
		
		//array of enrollees object
		enrollees = enrollees.map ( item => {
			return item.userId;
		})
	

		console.log(enrollees);
		return enrollees;
	
	}

})

.then(async (enrollees) => {

	// provide an exit route if the user is not an admin
	if (adminUser === 'false' || !adminUser) {
		return 0;
	}

	const promises = enrollees.map(async (item) => {
		console.log(item);

		const res = await fetch(`https://fathomless-chamber-66302.herokuapp.com/api/users/find/${item}`, {
			method: 'POST'
		})

		return res.json();

	})

	const result = await Promise.all(promises);
	console.log(result);
	return result;

})

.then(data => {
	let students = data.map(student => {
		return `${student.firstName} ${student.lastName}`
	})
	// output is an array of name strings
	console.log(students);
	return students

})

.then(students => {

courseEnrollees.innerHTML = '<p>Students:</p>'+ students.map( student => {
	return (
		`
			<p>${student}</p>

		`
	)
}).join('')

})


