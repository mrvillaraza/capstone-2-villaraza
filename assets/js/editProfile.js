let profileForm = document.querySelector('#profileForm');
let userId = localStorage.getItem('id');
let token = localStorage.getItem('token');

let firstName = document.querySelector('#firstName');
let lastName = document.querySelector('#lastName');
let email = document.querySelector('#email');
let mobileNo = document.querySelector('#mobileNumber');

let firstNameOld, lastNameOld, emailOld, mobileNoOld;

// fetch user info using backend .get method
fetch('https://fathomless-chamber-66302.herokuapp.com/api/users/details', {
	headers:
	{
		'Authorization': `Bearer ${token}`
	}

})
.then(res => res.json())
.then(data => {
	console.log(data);
	
	firstNameOld = data.firstName;
	lastNameOld = data.lastName;
	emailOld = data.email;
	mobileNoOld = data.mobileNo;

	firstName.placeholder = data.firstName;
	lastName.placeholder = data.lastName;
	email.placeholder = data.email;
	mobileNo.placeholder = data.mobileNo;

	profileForm.addEventListener("submit", (e) => {
		
		e.preventDefault();

		if (firstName.value === '') {
			firstName.value = firstNameOld;
		}

		if (lastName.value === '') {
			lastName.value = lastNameOld;
		}

		if (email.value === '') {
			email.value = emailOld;
		}

		if (mobileNumber.value === '') {
			mobileNo.value = mobileNoOld;
		}

		fetch('https://fathomless-chamber-66302.herokuapp.com/api/users', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json' ,
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				userId: userId,
				firstName: firstName.value,
				lastName: lastName.value,
				email: email.value,
				mobileNo: mobileNo.value
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if (data == true) {
				alert('Profile details have been updated!')
			} else {
				alert('Something went wrong!')
			}

			// reload edit page
			location.reload();
		})

	})

})	





	










