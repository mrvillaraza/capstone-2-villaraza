// let formSubmit = document.querySelector('#editCourse');
let params = new URLSearchParams(window.location.search);
let token = localStorage.getItem('token');

let courseId = params.get('courseId');
console.log(courseId);

fetch(`https://fathomless-chamber-66302.herokuapp.com/api/courses/${courseId}`, {
	method: 'DELETE',
	headers: {
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	console.log(data);

	if (data === true) {
		alert('Course is now archived');
		window.location.replace('courses.html');
	} else {
		alert('Something went wrong');
		window.location.replace('courses.html');
	}

})


