let navItems = document.querySelector('#navSession');

// localStorage = an object for storing information inside client device
let userToken = localStorage.getItem('token');
// console.log(userToken);

/*localStorage {
	getItem: function () => {
		print key
	}
}*/

if (!userToken) {
	
	// swap register with login when already at the login page
	if (document.querySelector('#logInUser') != null) {
		navItems.innerHTML = 
		`
			<li class = "nav-item">
				<a href = "./register.html" class = "nav-link">
					Register
				</a>
			</li>
		`
	} else {

		navItems.innerHTML = 
		`
			<li class = "nav-item">
				<a href = "./login.html" class = "nav-link">
					Login
				</a>
			</li>
		`
	}

	
} else {
	// user is logged in
	navItems.innerHTML =
		`
			<li class = "nav-item">
				<a href = "./logout.html" class = "nav-link">
					Logout
				</a>
			</li>
		`
}





