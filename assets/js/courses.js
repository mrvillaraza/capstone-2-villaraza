// retrieve the user information for isAdmin
let adminUser = localStorage.getItem('isAdmin');
let token = localStorage.getItem('token');

console.log(adminUser);

// will contain the html for the different buttons per user
let cardFooter;
let cardFooterEnable, cardFooterDisable;

// user is a normal user
if (adminUser == 'false' || !adminUser) {

	// retrieving all active courses
	fetch('https://fathomless-chamber-66302.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {
		
		console.log(data);

		// Variable to store the card to show if there's no courses
		let courseData;

		if (data.length < 1) {

			courseData = 'No courses available'

		} else {

			courseData = data.map(course => {
				console.log(course._id);
				
				// user can only enroll if they are logged in
				if (token != undefined) {
					cardFooter = 
					`
						<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton"> Select Course </a>
					`
				} else {
					
					cardFooter = 
					`
						<span></span>
					`
				}

				return (
					`
						<div class="col-md-6 my-3">
							<div class='card'>
								<div class='card-body'>
									<h5 class='card-title'>${course.name}</h5>
									<p class='card-text text-left'>
										${course.description}
									</p>
									<p class='card-text text-right'>
										₱ ${course.price}
									</p>

								</div>
									
								<div class='card-footer'>
									${cardFooter}
								</div>
					
							</div>
						</div>
			        `
				)

			}).join("")

			let coursesContainer = document.querySelector('#coursesContainer');
			coursesContainer.innerHTML = courseData;



		}
	})
} 

else {

// if user is an admin

	fetch('https://fathomless-chamber-66302.herokuapp.com/api/courses/courseLib')
	.then(res => res.json())
	.then(data => {
		
		console.log(data);

		// Variable to store the card to show if there's no courses
		let courseData;

		if (data.length < 1) {

			courseData = 'No courses available'

		} else {

			courseData = data.map(course => {
				console.log(course._id);
						
				cardFooter =
				`
					<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-warning text-white btn-block editButton"> View Course </a>
					<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton"> Edit </a>
				`

				cardFooterEnable =
				`
					<a href="./enableCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-success text-white btn-block editButton"> Enable Course </a>
				`

				cardFooterDisable =
				`
					<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-danger text-white btn-block dangerButton"> Disable Course </a>
				`

				// when isActive = true (active), provide Disable Button
				// when isActive = false (inactive), provide Enable Button

				if (course.isActive == true) {
					cardFooter = cardFooter + cardFooterDisable;
				}

				else {
					cardFooter = cardFooter + cardFooterEnable;
				}
	
			return (
					`
						<div class="col-md-6 my-3">
							<div class='card'>
								<div class='card-body'>
									<h5 class='card-title'>${course.name}</h5>
									<p class='card-text text-left'>
										${course.description}
									</p>
									<p class='card-text text-right'>
										₱ ${course.price}
									</p>

								</div>
									
								<div class='card-footer'>
									${cardFooter}
								</div>
					
							</div>
						</div>
			        `
				)

			}).join("")

			let coursesContainer = document.querySelector('#coursesContainer');

			coursesContainer.innerHTML = courseData;
		}
	})

}

let modalButton = document.querySelector('#adminButton');

if (adminUser == 'false' || !adminUser) {
	modalButton.innerHTML = null;
} else {
	modalButton.innerHTML = 
	`
	<div class="col-md-2 offset-md-10">
		<a href="./addCourse.html" class="btn btn-block btn-primary"> Add Course </a>
	</div>
	`
}

// Remove the profile tab on the navbar if the user is not logged in
if (token == undefined) {
	// swap profile into register tab instead
	let profileTab = document.querySelector('#profile-tab');
	profileTab.innerHTML =
		`
			<li class = "nav-item">
				<a href = "./register.html" class = "nav-link">
					Register
				</a>
			</li>
		`
}


