// extract logged-in user id
let userId = localStorage.getItem('id');
let profileToken = localStorage.getItem('token');
let profileUser;
let courseArray;

// fetch user info using backend .get method
fetch('https://fathomless-chamber-66302.herokuapp.com/api/users/details', {
	headers:
	{
		'Authorization': `Bearer ${profileToken}`
	}

})
.then(res => res.json())
.then(data => {
	console.log(data);
	// data contains the user details
	
	profileUser = data;
	// user details is now saved within a variable "profileUser"
	console.log(profileUser);

	// check for enrolled course
	if (data.enrollments.length < 1) {
		courseArray = [];
	} else { // retrieve course name from course ids (course .get method)
		// for each courseId retreive the corresponding course name
		courseArray = data.enrollments;
		console.log(courseArray);

		courseArray = courseArray.map(course => {
			return course.courseId
		})

	}

	console.log(courseArray);
	return(courseArray);
})

.then(async (array) => {
 
	const promises = array.map( async(item) => {
		
		console.log(item);

		const res = await fetch(`https://fathomless-chamber-66302.herokuapp.com/api/courses/${item}`, {
			method: 'POST'
		})

		return res.json();

	})

	const result = await Promise.all(promises);
	console.log(result);
	return result;
})

.then(out => {
	console.log(out);

	let profileCourse;

	if (out.length < 1) {

		profileCourse = '- No Enrolled Course -';

	} else {

		profileCourse = [];
		
		out.forEach(course => {
			let courseEntry;

			courseEntry = 
			`
				<div class="py-2 row">
				<span class="text-primary profile-text col-md-4">${course.name}</span>
				<span class="text-primary profile-text col-md-4">${profileUser.enrollments[out.indexOf(course)].enrolledOn}</span>
				<span class="text-primary profile-text col-md-4">${profileUser.enrollments[out.indexOf(course)].status}</span>
				</div>
			`

			profileCourse.push(courseEntry);

		});

		let courseHeader =
			`
				<hr>
				<div class="row">
				<span class="text-primary profile-text col-md-4">Course Name</span>
				<span class="text-primary profile-text col-md-4">Enrolled On:</span>
				<span class="text-primary profile-text col-md-4">Status</span>
				</div>
				<hr>
			`

		profileCourse = courseHeader + profileCourse.join('');

		console.log(profileCourse);
	}

	let profileInfo = 
	`
		<div class="col-md-12">
			<section class="jumbotron text-center my-5">		
				<h2 class="my-5">
					<p class = 'text-primary profile-text'> First Name : ${profileUser.firstName}</p>
					<p class = 'text-primary profile-text'> Last Name : ${profileUser.lastName}</p>
					<p class = 'text-primary profile-text'> Email : ${profileUser.email}</p>
					<p class = 'text-primary profile-text'> Mobile # : ${profileUser.mobileNo}</p>
					<p class = 'text-primary profile-text'>${profileCourse}</p>
				</h2>
			</section>
		</div>

	`

	let profileContainer = document.querySelector('#profileContainer');

	profileContainer.innerHTML = profileInfo;

})

